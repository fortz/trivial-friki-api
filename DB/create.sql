-- phpMyAdmin SQL Dump
-- version 4.0.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 25, 2015 at 08:13 PM
-- Server version: 5.5.33
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `Trivial_Friki`
--

-- --------------------------------------------------------

--
-- Table structure for table `Answer`
--

CREATE TABLE `Answer` (
  `Answer_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Text` varchar(2000) NOT NULL,
  `Type` enum('text','audio','image','video') NOT NULL,
  PRIMARY KEY (`Answer_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Category`
--

CREATE TABLE `Category` (
  `Category_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(100) NOT NULL,
  PRIMARY KEY (`Category_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Category_Question`
--

CREATE TABLE `Category_Question` (
  `Category_ID` int(11) NOT NULL,
  `Question_ID` int(11) NOT NULL,
  PRIMARY KEY (`Category_ID`,`Question_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Game_Question`
--

CREATE TABLE `Game_Question` (
  `Game_ID` int(11) NOT NULL,
  `Question_ID` int(11) NOT NULL,
  PRIMARY KEY (`Game_ID`,`Question_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Game_User`
--

CREATE TABLE `Game_User` (
  `Game_ID` int(11) NOT NULL,
  `User_ID` int(11) NOT NULL,
  PRIMARY KEY (`Game_ID`,`User_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Question`
--

CREATE TABLE `Question` (
  `Question_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Type` enum('text','audio','image','video') NOT NULL,
  `Text` varchar(2000) NOT NULL,
  `Proposer_User_ID` int(11) NOT NULL,
  PRIMARY KEY (`Question_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Question_Answer`
--

CREATE TABLE `Question_Answer` (
  `Question_ID` int(11) NOT NULL,
  `Answer_ID` int(11) NOT NULL,
  `Correct` tinyint(1) NOT NULL,
  PRIMARY KEY (`Question_ID`,`Answer_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Question_User`
--

CREATE TABLE `Question_User` (
  `Question_ID` int(11) NOT NULL,
  `User_ID` varchar(100) NOT NULL,
  `Approved` tinyint(1) NOT NULL,
  PRIMARY KEY (`Question_ID`,`User_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla que relaciona las preguntas votadas por los usuarios';

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `User_ID` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  PRIMARY KEY (`User_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
